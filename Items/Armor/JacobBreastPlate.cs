﻿using System;
using Terraria.ID;
using Terraria.ModLoader;

namespace GodJoey.Items.Armor {
    [AutoloadEquip(EquipType.Body)]
    public class JacobBreastPlate : ModItem {
        public override void SetStaticDefaults() {
            base.SetStaticDefaults();
            DisplayName.SetDefault("Jacob Breast Plate");
            Tooltip.SetDefault("Jacob at his best!");
        }

        public override void SetDefaults() {
            item.width = 23;
            item.height = 29;
            item.value = Int16.MaxValue;
            item.rare = 2;
            item.defense = Int16.MaxValue;
        }

        public override void AddRecipes() {
            var recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.DirtBlock);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}