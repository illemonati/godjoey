﻿using System;
using Terraria.ID;
using Terraria.ModLoader;

namespace GodJoey.Items {
    public class GodJoeySword : ModItem {
        public override void SetStaticDefaults() {
            Tooltip.SetDefault("GodJoeySword");
        }

        public override void SetDefaults() {
            item.damage = Int16.MaxValue;
            item.melee = true;
            item.width = 250;
            item.height = 356;
            item.useTime = 20;
            item.useAnimation = 20;
            item.useStyle = 1;
            item.knockBack = 6;
            item.value = 10000;
            item.rare = 2;
            item.UseSound = SoundID.Item1;
            item.autoReuse = true;
        }

        public override void AddRecipes() {
            var recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Wood);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}