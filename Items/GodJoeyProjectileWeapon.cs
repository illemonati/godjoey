﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace GodJoey.Items {
    public class GodJoeyProjectileWeapon : ModItem {
        public override void SetStaticDefaults() {
            Tooltip.SetDefault("GodJoeyProjectileWeapon");
        }

        public override void SetDefaults() {
            item.ranged = true;
            item.width = 250;
            item.height = 356;
            item.useTime = 20;
            item.useAnimation = 20;
            item.useStyle = 5;
            item.noMelee = true;
            item.knockBack = 6;
            item.damage = Int16.MaxValue;
            item.value = 10000;
            item.rare = 2;
            item.shootSpeed = 16f;
            item.shoot = mod.ProjectileType("GodJoeyProjectile");
            item.UseSound = SoundID.Item1;
            item.autoReuse = true;
        }

        public override bool AltFunctionUse(Player player) {
            return true;
        }

        public override bool CanUseItem(Player player) {
            if (player.altFunctionUse == 2) {
                item.damage = Int16.MinValue;
                
            }
            else {
                item.damage = Int16.MaxValue;
            }

            return true;
        }


        public override bool Shoot(Player player, ref Vector2 position, ref float speedX, ref float speedY,
            ref int type, ref int damage, ref float knockBack) {
            
            int numberProjectiles = 10 + Main.rand.Next(2); // 4 or 5 shots
            for (int i = 0; i < numberProjectiles; i++) {
                Vector2 perturbedSpeed =
                    new Vector2(speedX, speedY).RotatedByRandom(MathHelper.ToRadians(30)); // 30 degree spread.
                Projectile.NewProjectile(position.X, position.Y, perturbedSpeed.X, perturbedSpeed.Y, type, damage,
                    knockBack, player.whoAmI);
            }

            return false; // return false because we don't want tmodloader to shoot projectile
        }

        public override void AddRecipes() {
            var recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.Wood);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}