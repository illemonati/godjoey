﻿using System;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace GodJoey.Items {
    class GodJoeyProjectile : ModProjectile {
        public override void SetStaticDefaults() {
            DisplayName.SetDefault("GodJoey Projectile");
        }

        public override void SetDefaults() {
            projectile.arrow = true;
            projectile.width = 50;
            projectile.height = 50;
            projectile.aiStyle = 1;
            projectile.friendly = true;
            projectile.ranged = true;
            projectile.maxPenetrate = Int16.MaxValue;
            
            aiType = ProjectileID.WoodenArrowFriendly;
        }

        public override bool OnTileCollide(Vector2 oldVelocity) {
            projectile.velocity = oldVelocity.RotateRandom(Math.PI);
            return false;
        }


        public override void AI() {
            Lighting.AddLight(projectile.position, 1, 1, 1);
        }
    }
}